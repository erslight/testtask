package berthold.testtask.base;

/**
 * Created by User on 23.03.2017.
 */

public interface Presenter {

    void onCreate();

    void onPause();

    void onResume();

    void onDestroy();
}
