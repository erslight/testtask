package berthold.testtask.base;

import berthold.testtask.metadata.MetaData;
import berthold.testtask.service.DataViewInterface;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.ResourceObserver;

/**
 * Created by User on 23.03.2017.
 */

public class DataPresenter extends BasePresenter implements Observer<MetaData> {

    private DataViewInterface viewInterface;

    public DataPresenter(DataViewInterface viewInterface) {
        this.viewInterface = viewInterface;
    }


    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(MetaData value) {

    }

    @Override
    public void onError(Throwable e) {
        viewInterface.onError(e.getMessage());
    }

    @Override
    public void onComplete() {
        viewInterface.onCompleted();
    }

}
