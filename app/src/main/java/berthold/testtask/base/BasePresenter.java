package berthold.testtask.base;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Emitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.ResourceObserver;
import io.reactivex.schedulers.Schedulers;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.observers.Subscribers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by User on 23.03.2017.
 */

public abstract class BasePresenter implements Presenter {

    private CompositeDisposable compositeSubscription;

    @Override
    public void onCreate() {
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

        configureSubscription();
    }

    private CompositeDisposable configureSubscription() {

        if (compositeSubscription == null || !compositeSubscription.isDisposed()) {
            compositeSubscription = new CompositeDisposable();
        }
        return compositeSubscription;
    }


    @Override
    public void onDestroy() {

        unsubscribeAll();
    }

    protected void unsubscribeAll() {
        if(compositeSubscription != null) {
            compositeSubscription.clear();
        }
    }

    protected <T> void subscribe(io.reactivex.Observable<T> observable, ResourceObserver<T> observer) {


        configureSubscription().add(observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.computation())
                .subscribeWith(observer)
        );
    }
}
