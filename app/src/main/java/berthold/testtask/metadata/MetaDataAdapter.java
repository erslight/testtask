package berthold.testtask.metadata;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import berthold.testtask.R;

/**
 * Created by User on 23.03.2017.
 */

public class MetaDataAdapter extends RecyclerView.Adapter<MetaDataAdapter.Holder> {


    private final LayoutInflater inflater;
    private List<Field> fieldList;

    public MetaDataAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        fieldList = new ArrayList<>();

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.item_data, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return fieldList.size();
    }

    public void addData(List<Field> fields) {
        fieldList.addAll(fields);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        public Holder(View itemView) {
            super(itemView);
        }
    }
}
