package berthold.testtask.dependecies;

import berthold.testtask.ui.MainActivity;
import dagger.Component;

/**
 * Created by User on 23.03.2017.
 */

@CustomScope
@Component(modules = ApiModule.class, dependencies = NetworkComponent.class)
public interface ApiComponent {

    void inject(MainActivity activity);
}
