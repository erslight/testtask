package berthold.testtask.dependecies;

import berthold.testtask.service.ClevertecApi;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by User on 23.03.2017.
 */

@Module
public class ApiModule {

    @Provides
    @CustomScope
    ClevertecApi provideMetaData(Retrofit retrofit) {
        return retrofit.create(ClevertecApi.class);
    }
}
