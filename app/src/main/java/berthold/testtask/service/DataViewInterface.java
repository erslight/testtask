package berthold.testtask.service;

import java.util.List;

import berthold.testtask.metadata.Field;
import berthold.testtask.metadata.MetaData;
import io.reactivex.Observable;

/**
 * Created by User on 23.03.2017.
 */

public interface DataViewInterface {
    void onCompleted();


    void onError(String message);

    void onData(List<Field> fields);

    Observable<MetaData> getData() ;
}
