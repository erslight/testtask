package berthold.testtask.service;

import berthold.testtask.metadata.MetaData;
import io.reactivex.Observable;
import retrofit2.http.POST;

/**
 * Created by User on 19.03.2017.
 */

public interface ClevertecApi {

    @POST("meta")
    Observable<MetaData> getMetaData();


}