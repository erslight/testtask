package berthold.testtask;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import berthold.testtask.service.ClevertecApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 19.03.2017.
 */

public class App extends Application {

    private static ClevertecApi clevertecApi;

    private static final String BASE_URL = "http://test.clevertec.ru/tt/";
    private static final String UMO_URL = "http://www.umori.li/";


    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    @Override
    public void onCreate() {
        super.onCreate();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        clevertecApi = retrofit.create(ClevertecApi.class);


    }

    public static ClevertecApi getClevertecApi() {
        return clevertecApi;
    }

}
