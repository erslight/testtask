package berthold.testtask.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import berthold.testtask.R;
import berthold.testtask.application.DataApplication;
import berthold.testtask.base.DataPresenter;
import berthold.testtask.metadata.Field;
import berthold.testtask.metadata.MetaData;
import berthold.testtask.metadata.MetaDataAdapter;
import berthold.testtask.service.ClevertecApi;
import berthold.testtask.service.DataViewInterface;
import butterknife.Bind;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class MainActivity extends AppCompatActivity implements DataViewInterface{

    private static final String TAG = "myLogs";

    private DataPresenter presenter;

    private ProgressDialog progressDialog;


    @Inject
    ClevertecApi clevertecApi;

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    private MetaDataAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resolveDependency();

        ButterKnife.bind(MainActivity.this);
        configView();
        
        presenter = new DataPresenter(MainActivity.this);
        presenter.onCreate();



    }

    private void resolveDependency() {
        ((DataApplication) getApplication())
                .getComponent()
                .inject(MainActivity.this);
    }

    private void configView() {
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        adapter = new MetaDataAdapter(getLayoutInflater());
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Downloading data");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    @Override
    public void onCompleted() {
        progressDialog.dismiss();
    }

    @Override
    public void onError(String message) {

        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onData(List<Field> fields) {
        adapter.addData(fields);
    }

    @Override
    public Observable<MetaData> getData() {
        return clevertecApi.getMetaData();
    }
}