package berthold.testtask.application;

import android.app.Application;

import berthold.testtask.dependecies.ApiComponent;
import berthold.testtask.dependecies.DaggerApiComponent;
import berthold.testtask.dependecies.DaggerNetworkComponent;
import berthold.testtask.dependecies.NetworkComponent;
import berthold.testtask.dependecies.NetworkModule;

/**
 * Created by User on 23.03.2017.
 */

public class DataApplication extends Application {

    private ApiComponent component;


    public void OnCreate() {
        resolveDependency();
        super.onCreate();
    }

    private void resolveDependency() {
        component = DaggerApiComponent.builder()
                .networkComponent(getNetworkComponent())
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule())
                .build();
    }

    public ApiComponent getComponent() {
        return component;
    }
}
